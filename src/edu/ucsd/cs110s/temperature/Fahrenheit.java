package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
	
	public Fahrenheit(float t){
		super(t);
	}
	public String toString(){
		float value = this.getValue();
		return String.valueOf(value);
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float value = this.getValue();
		value = (value - 32)  * 5 / 9 ;
		return new Celsius(value);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float value = this.getValue();
		return new Fahrenheit(value);
	}

}
