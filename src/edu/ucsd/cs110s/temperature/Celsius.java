package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	
	public Celsius(float t){
		super(t);
	}
	public String toString(){
		float value = this.getValue();
		return String.valueOf(value);
	}
	@Override
	public Temperature toCelsius() {
		float value = this.getValue();
		return new Celsius(value);
	}
	@Override
	public Temperature toFahrenheit() {
		float value = this.getValue();
		value = value * 9  /  5 + 32;
		return new Fahrenheit(value);
	}

}
